<?php
final class Pagination {
    // total items to "paginate" (does this word actually exist?)
    protected static $items;

    // items per page
    protected static $items_per_page;
    
    // current page
    protected static $current_page;
    
    /**
     * set total items
     * @param int $items
     * @return void 
     */
    public static function setTotalItems($items) {
        if(is_numeric($items))
            self::$items = $items;
        else
            self::$items = 0;
        
        return true;
    }
    
    /**
     * set items per page
     * @param int $items
     * @return void 
     */
    public static function setItemsPerPage($items) {
        if(is_numeric($items))
            self::$items_per_page = $items;
        else
            self::$items_per_page = 0;

        return true;
    }
    
    /**
     * returns items per page
     * @return int
     */
    public static function getItemsPerPage() {
        return self::$items_per_page;
    }
    
    /**
     * returns current page
     * @global int $_GET
     * @return int 
     */
    public static function getCurrentPage() {
        global $_GET;

        if(!isset($_GET['page']))
            $page = 1;
        else {
            $p = ($_GET['page']);
            if(is_numeric($p))
                $page = $p;
            else
                $page = 1;
            
        }

        self::$current_page = $page;
        return self::$current_page;
    }
    
    /**
     * calculate sql start limit
     * @return int
     */
    public static function limitStart() {
        if(self::$current_page == 1) {
            $limitStart = 0;
        } else {
            $limitStart = (self::$items_per_page * self::getCurrentPage()) - self::$items_per_page;

            if($limitStart >= self::limitEnd())
                $limitStart = self::$items - self::$items_per_page;
        }

        return $limitStart;
    }
    
    /**
     * function to limitate sql queries
     * @return string
     */
    public static function SQLLimits() {
        return Pagination::limitStart().",".Pagination::getItemsPerPage();
    }
    
    /**
     * creates a pagination link
     * @param int $n
     * @param string $caption
     * @param string $class
     * @return string 
     */
    public static function createPageLink($n, $caption = '', $disabled = false) {
        $qs = $_SERVER['QUERY_STRING'];
        
        $splitArgs = explode('&', $qs);
        $elem = 0;
        foreach($splitArgs as $args) {
            if(substr($args, 0, 4) == 'page')
                unset($splitArgs[$elem]);

            $elem++;
        }
        $qs = implode('&', $splitArgs);
        if($qs != '')
            $url = $_SERVER['PHP_SELF'].'?'.$qs.'&page='.$n;
        else
            $url = $_SERVER['PHP_SELF'].'?page='.$n;
        
        if($disabled)
            return '<li class="disabled"><a href="#">'.$caption.'</a></li>';
        else
            return ' <li><a href="'.$url.'">'.$caption.'</a></li> ';
    }
    
    /**
     * calculates sql end limit
     * @return int
     */
    public static function limitEnd() {
        if(self::$current_page == 1)
            $limitEnd = self::$items_per_page;
        else {
            $limitEnd = (self::$items_per_page * self::$current_page);
            if($limitEnd > self::$items)
                $limitEnd = self::$items;
        }

        return $limitEnd;
    }
    
    /**
     * returns maximum pages
     * @return int
     */
    public static function getMaxPages() {
        $items = self::$items;
        $items_per_page = self::$items_per_page;

        return ceil($items / $items_per_page);
    }

    /**
     * creates pagination navigation.
     * @return string 
     */
    public static function getPageLinks() {
        $return = '';

        // @todo: exclude $pagination
        $pagination = array(1, 2, 10, 50, 100);
        $selected_pages = array();
        array_push($selected_pages, self::getCurrentPage());
        foreach($pagination as $p) {
            if((self::getCurrentPage() - $p <= self::getMaxPages()) && (self::$current_page - $p > 0))
                array_push($selected_pages, self::$current_page - $p);
            if((self::getCurrentPage() + $p <= self::getMaxPages()) && (self::$current_page + $p > 0))
                array_push($selected_pages, self::$current_page + $p);
        }

        sort($selected_pages);

        foreach($selected_pages as $p) {
            if($p == self::$current_page)
                $return .= '<li class="active"><a href="#">'.$p.'</a></li>';
            else
                $return .= self::createPageLink($p, $p);
        }

        return $return;
    }
    
    /**
     * returns first page link
     * @return string
     */
    public static function getFirstPageLink() {
        if(self::getCurrentPage() != 1)
            return self::createPageLink(1, 'Erste').PHP_EOL;
        else
            return self::createPageLink(1, 'Erste', true).PHP_EOL;
    }

    /**
     * returns last page link
     * @return string
     */
    public static function getLastPageLink() {
        if(self::getCurrentPage() != self::getMaxPages())
            return self::createPageLink(self::getMaxPages(), 'Letzte').PHP_EOL;
        else
            return self::createPageLink(self::getMaxPages(), 'Letzte', true).PHP_EOL;
    }

    /**
     * returns previous page link
     * @return string
     */
    public static function getPrevPageLink() {
        if(self::getCurrentPage() == 1) {
            return self::createPageLink(self::$current_page - 1, 'Vorherige', true);
        } else {
            return self::createPageLink(self::$current_page - 1, 'Vorherige');
        }
    }

    /**
     * returns next page link
     * @return type 
     */
    public static function getNextPageLink() {
        if(self::getMaxPages() == self::getCurrentPage()) {
            return self::createPageLink(self::$current_page + 1, 'Nächste', true);
        } else {
            return self::createPageLink(self::$current_page + 1, 'Nächste');
        }
    }

    /**
     * returns complete page navigation
     * @return string
     */
    public static function pageNavigation() {
        if(self::$items > 0 && self::$items_per_page > 0)
            return self::getFirstPageLink().' '.
                   self::getPrevPageLink().' '.
                   self::getPageLinks().' '.
                   self::getNextPageLink().' '.
                   self::getLastPageLink();
    }
}