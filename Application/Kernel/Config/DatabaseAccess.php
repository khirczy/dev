<?php
/**
 * Database access data
 * 
 * @package Configuration
 * @since 1.0.0
 */

if($loadDbAccess) {
    return array(
        /** Hostname */
        'host' => 'localhost',
        /** Username */
        'user' => 'root',
        /** Password */
        'pass' => '',
        /** Databasename */
        'database' => 'dev'
    );
}
?>