<?php
/**
 * Common constants
 * 
 * @package Configuration
 * @since 1.0.0
 */

/* ----------------------------------------------------
 * Common constants 
 * --------------------------------------------------*/
/** IP of Client */
define('CLIENT_IP', $_SERVER['REMOTE_ADDR']);
/** Current date in format d.m.Y H:i:s */
define('CURRENT_DATE', date('d.m.Y H:i:s'));
/** Current request uri */
define('REQUEST_URI', $_SERVER['REQUEST_URI']);
/** Current timestamp */
define('CURRENT_TIMESTAMP', time());

/* ----------------------------------------------------
 * Priority constants for error triggering
 * --------------------------------------------------*/
/** High priority */
define('PRIO_H', 1);
/** Low priority */
define('PRIO_L', 0);

/* ----------------------------------------------------
 * Environment constants
 * --------------------------------------------------*/
/** Development environment */
define('ENV_DEVELOPMENT', 0);
/** Release environment */
define('ENV_RELEASE', 1);

/* ----------------------------------------------------
 * Version constants
 * --------------------------------------------------*/
define('VERS_STABLE', 0);
define('VERS_BETA', 1);
define('VERS_ALPHA', 2);

/* ----------------------------------------------------
 * Constants for system
 * --------------------------------------------------*/
/** Hook position - Begin of script */
define('HOOKP_BOS', 0);
/** Hook position - End of script */
define('HOOKP_EOS', 1);

/* ----------------------------------------------------
 * Constants for form submission
 * --------------------------------------------------*/
/** Submission types */
define('FRM_SUBM_SUCCESS', 0);
define('FRM_SUBM_ERROR', 1);
define('FRM_SUBM_WARNING', 2);

/* ----------------------------------------------------
 * User constants
 * --------------------------------------------------*/
/** Status types */
define('USR_BANNED', -1);
define('USR_NOTACTIVATED', 0);
define('USR_REGISTERED', 1);
define('USR_MODERATOR', 2);
define('USR_ADMINISTRATOR', 3);
/** Master user name */
define('USR_ROOT_NAME', 'root');
/** Master user ID (= max of int) */
define('USR_ROOT_ID', 2147483647);
/** Cookie runtime */
define('USR_CRT', CURRENT_TIMESTAMP + 86400);