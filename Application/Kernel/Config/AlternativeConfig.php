<?php
return array(
    'acp_login_locktime'    => 30,
    'acp_login_logtime'     => 14400,
    'acp_login_max_tries'   => 5,
    'root_pw'               => '098f6bcd4621d373cade4e832627b4f6',
    'ulog_items_per_page'   => 30,
    'users_per_page'        => 50,
    'file_validation_exclude' => '.git, Test'
);