<?php
/**
 * Configuration
 * 
 * @package Configuration
 * @since 1.0.0
 */

if($loadConfig) {
    return array(
        // Global settings
        'global' => array(
            // Environment (ENV_DEVELOPMENT or ENV_RELEASE)
            'environment' => ENV_DEVELOPMENT,
            // Required PHP Version for this software
            'required_php_version' => '5.3.0',
            // Execution limit
            'execution_limit' => 30,
            // Maximum of input vars
            'max_input_vars' => 10,
            // Memory limit
            'memory_limit' => '16M',
            // Default timezone
            'default_timezone' => 'Europe/Dublin'
        ),
        
        // Controller settings
        'controller' => array(
            // Available languages
            'languages' => array(
                'de'
            ),
            // Default language
            'default_language' => 'de', // TODO: exclude to registry
            
            // Cache
            'cache' => array(
				// no cache, no enabled ?
                'enabled' => true
            )
        ),
        
        // Error settings
        'error' => array(
            // Enable notification
            'notification' => true,
            // Mail for notificiations
            'notification_mail' => 'err@supp.com',
            // Error log file
            'logfile' => DIR_APP . DS . 'Kernel/Logs/Error.log',
            // Error log file string
            'logfile_string' => 'date|25 ip|15 type|8 url|45 msg|0',
            // Halt template
            'halt_template' => DIR_APP . DS . 'Kernel/HaltTemplate.php',
        ),
        
        // version information
        'version' => array(
            'major' => '0',
            'minor' => '0',
            'patch' => '8',
            'release' => VERS_ALPHA,
            'name' => 'Nunu' // TODO: think of a good name?
        ),
    );
}