<?php if(!defined('ROOT')) header('Location: ../../index.php');

/**
 * User
 * 
 * Responsible for everything related with users (NOT for login or registration)
 * 
 * @author Kevin Hirczy
 * @copyright 2012
 * @package Kernel
 * @subpackage User
 * @since 1.0.0
 * @version 1.0.0
 */
class User extends Database implements DBData {
    private $m_UserID;
    private $m_UserPwd;
    
    private $m_IsLogged = false;
    
    public function deleteCookies() {
        unset($_COOKIE);
        setcookie('id', '');
        setcookie('pwd', '');
    }
    
    public function isLogged() {
        if($this->m_IsLogged)
            return true;
        else
            return false;
    }
    
    public function renameUser($user, $name) {
        if(is_numeric($user)) {
            /* edit user by id */
            parent::query("SELECT name
                           FROM ".DBData::TB_USERS."
                           WHERE id = '".$user."'");
            if(parent::isAffected()) {
                parent::update(DBData::TB_USERS, array('name' => $name), array('id' => $user));
                return true;
            } else {
                Error::trigger('USR', 'Failed to rename user with id "'.$user.'"', PRIO_L);
                return false;
            }
        } else {
            /* edit user by name */
            parent::query("SELECT name
                           FROM ".DBData::TB_USERS."
                           WHERE name = '".$user."'");
            if(parent::isAffected()) {
                parent::update(DBData::TB_USERS, array('name' => $name), array('name' => $user));
                return true;
            } else {
                Error::trigger('USR', 'Failed to rename user with id "'.$user.'"', PRIO_L);
                return false;
            }
        }
    }
    
    public function deleteUser($user) {
        if(is_numeric($user))
            parent::delete(DBData::TB_USERS, array('id' => $user));
        else
            parent::delete(DBData::TB_USERS, array('name' => $user));
        
        if(parent::isAffected())
            return true;
        else {
            Error:trigger('USR', 'Failed to delete user ("'.$user.'"', PRIO_L);
            return false;
        }
    }
    
    public function editUser($user, array $data) {
        if(is_numeric($user))
            $use = 'id';
        else
            $use = 'name';
        
        parent::query("SELECT ".$use."
                       FROM ".DBData::TB_USERS."
                       WHERE ".$use." = '".$user."'");
        if(parent::isAffected()) {
            parent::update(DBData::TB_USERS, $data, array($use => $user));
            if(parent::isAffected())
                return true;
            else {
                Error::trigger('USR', 'Failed to update user data', PRIO_L);
                return false;
            }
        }
    }
    
    public function isRoot() {
        if($this->m_UserID == USR_ROOT_ID)
            return true;
        else
            return false;
    }
    
    private function validateUser() {
        if(!$this->isRoot()) {
            if(is_numeric($this->m_UserID) && trim($this->m_UserPwd) != '') {
                parent::query("SELECT password, status
                               FROM ".DBData::TB_USERS."
                               WHERE id = '".parent::escape($this->m_UserID)."'");

                if(parent::isAffected()) {
                    $row = parent::fetchObj();

                    if($row->password === $this->m_UserPwd) {
                        $this->m_IsLogged = true;

                    } else {
                        $this->m_IsLogged = false;
                        Error::trigger('USR', 'User ('.$this->m_UserID.') used faked cookies', PRIO_L);
                        $this->deleteCookies();
                        // TODO: force reload
                    }
                } else {
                    $this->m_IsLogged = false;
                    $this->deleteCookies();
                }
            }
        } else {
            if($this->m_UserPwd == Kernel::$Registry->get('root_pw')) {
                $this->m_IsLogged = true;
            } else {
                $this->m_IsLogged = false;
                Error::trigger('USR', 'Client ('.CLIENT_IP.') faked root access', PRIO_L);
                Kernel::$System->delCookies(array(
                    'id', 'pwd', 'acp_access', 'root'
                ));
            }
        }
    }
    
    public function get($key, $uid = null) {
        if($uid == null)
            $uid = $this->m_UserID;
        
        parent::query("SELECT ".$key."
                       FROM ".DBData::TB_USERS."
                       WHERE id = '".$uid."'");
        if(parent::isAffected()) {
            $row = parent::fetchObj();
            return $row->$key;
        } else {
            if($this->isRoot())
                return 'root';
            else
                return "n/a"; // TODO: trigger error
        }
    }
    
    public function getStatus($uid = null) {
        if($uid == null)
            $uid = $this->m_UserID;
        
        parent::query("SELECT status
                       FROM ".DBDATA::TB_USERS."
                       WHERE id = '".$uid."'");
        if(parent::isAffected()) {
            $row = parent::fetchObj();
            return $row->status;
        } else {
            if($this->isRoot())
                return USR_ADMINISTRATOR;
            else
                return 0;
        }
    }
    
    public function getID($uid = null) {
        return $this->m_UserID;
    }
    
    public function __construct() {
        if(isset($_COOKIE['id']) && isset($_COOKIE['pwd'])) {
            $this->m_UserID = $_COOKIE['id'];
            $this->m_UserPwd = $_COOKIE['pwd'];
            
            $this->validateUser();
        }
    }
}