<?php if(!defined('ROOT')) header('Location: ../../index.php');

/**
 * Registry
 * 
 * This class handles everything associated with the registry
 * 
 * @author Kevin Hirczy
 * @copyright 2012
 * @package Kernel
 * @subpackage Registry
 * @since 1.0.0
 * @version 1.0.0
 */
final class Registry extends Database implements DBData {
    private $m_ItemStorage = array();
    
    /**
     * Sets a registry item
     * If the item does not exist it will be created
     * @param string $name
     * @param string $value 
     */
    public function set($name, $value) {
        parent::query("SELECT name, value
                       FROM ".DBData::TB_REGISTRY."
                       WHERE name = '".parent::escape($name)."'");

        if(parent::isAffected()) {
            $row = parent::fetchObj();
            
            if($row->value != $value) { /* to make it A LOT faster */
                /* edit item */
                Kernel::$Log->record('REG', 'Edited item ('.$name.')');
                parent::update(DBData::TB_REGISTRY, array(
                    'value' => $value
                    ), array(
                        'name'  => $name
                    )
                );
            }
        } else {
            /* add item */
            Kernel::$Log->record('REG', 'Added item ('.$name.')');
            parent::insert(DBData::TB_REGISTRY, array(
                'name'  => $name,
                'value' => $value
            ));
        }
    }
    
    /**
     * Returns value of a registry item
     * @param string $name
     * @return string
     */
    public function get($name) {
        parent::query("SELECT value
                       FROM ".DBData::TB_REGISTRY."
                       WHERE name = '".parent::escape($name)."'");
        
        if(parent::isAffected()) {
            $row = parent::fetchObj();
            return $row->value;
        } else {
            Error::trigger('REG', 'Item ('.$name.') not found.', PRIO_L);
            
            $alternative = require DIR_APP . DS . DIR_KERNEL . DS . DIR_CONFIG . DS . 'AlternativeConfig.php';            
            if(isset($alternative[$name]))
                return $alternative[$name];
            else
                return 0;
        }
    }
    
    /**
     * Deletes an item from the registry
     * @param string $name
     * @return boolean 
     */
    public function del($name) {
        parent::query("SELECT name
                       FROM ".DBData::TB_REGISTRY."
                       WHERE name = '".parent::escape($name)."'");
        
        if(parent::isAffected()) {
            Kernel::$Log->record('REG', 'Deleted item ('.$name.')');
            parent::delete(DBData::TB_REGISTRY, array('name' => $name));
            return true;
        } else {
            Error::trigger('REG', 'Item ('.$name.') could not be removed.', PRIO_L);
            return false;
        }
    }
}