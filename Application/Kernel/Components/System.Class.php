<?php
class System {
    private $m_Hooks = array();
    
    public function addHook($function, $position) {
        if($position == HOOKP_BOS || $position == HOOKP_EOS) {
            $this->m_Hooks[$function] = $position;
        } else Error::trigger('SYS',
                              'Unknown hook position of '.$function, PRIO_H);
    }
    
    public function executeHooks($position) {
        foreach($this->m_Hooks as $function => $pos) {
            if($pos == $position) {
                call_user_func($function);
            }
        }
    }
    
    public function listHooks() {
        return $this->m_Hooks;
    }
    
    public function catchGetVar($name, $value = '') {
        if($value == '') {
            if(isset($_GET[$name]))
                return $_GET[$name];
            else
                return false;
        } else {
            if(isset($_GET[$name]) && $_GET[$name] == $value)
                return true;
            else
                return false;
        }
    }
    
    public function catchPostEvent($event) {
        if(isset($_POST[$event]))
            return true;
        else
            return false;
    }
    
    public function getPostValue($value) {
        if($this->catchPostEvent($value))
            return $_POST[$value];
        else
            return false;
    }
    
    public function setCookies(array $cookies, $expire) {
        foreach($cookies as $name => $value) {
            setcookie($name, $value, time() + $expire);
        }
    }
    
    public function delCookies(array $cookies) {
        foreach($cookies as $name) {
            setcookie($name, '');
        }
    }
}