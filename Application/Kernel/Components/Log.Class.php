<?php if(!defined('ROOT')) header('Location: ../../index.php');

/**
 * Log
 * 
 * Class for logging actions.
 * 
 * @author Kevin Hirczy
 * @copyright 2012
 * @package Kernel
 * @subpackage Log
 * @since 1.0.0
 * @version 1.0.0
 */
class Log extends Database implements DBData {
    /**
     * Function to record an action
     * @example record('USR', 'Did something good.', 15);
     * @param type $section
     * @param type $action
     * @param int $uid If null the id will be 0 (for system)
     */
    public function record($section, $action, $uid = null) {
        if($action != '' && $section != '') {
            if($uid == null)
                $uid = 0;
            
            parent::insert(DBData::TB_LOG, array(
                'uid' => $uid,
                'time' => time(),
                'action' => $action,
                'ip' => CLIENT_IP,
                'section' => $section
            ));
        } else
            Error::trigger($section, 'Logging action ('.$action.') failed',
                           PRIO_L);
    }
}