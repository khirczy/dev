<?php if(!defined('ROOT')) header('Location: ../../index.php');

/**
 * Error management
 * 
 * Class for error management.
 * 
 * @author Kevin Hirczy
 * @copyright 2012
 * @package Kernel
 * @subpackage Error
 * @since 1.0.0
 * @version 1.0.0
 */
final class Error implements ConfigDependency {
    /**
     * Configuration
     * @var array
     */
    private static $m_Configuration;
    /**
     * Logfile path, based on config
     * @var string
     */
    public static $m_LogFile;
    
    /**
     * Internal storage for error sections
     * @var string
     */
    private static $m_ErrSection;
    /**
     * Internal storage for error messages
     * @var string
     */
    private static $m_ErrMsg;
    
    /**
     * Sets the configuration
     * @param array $config 
     */
    public static function setConfiguration(array $config) {
        self::$m_Configuration = $config;
    }
    
    /**
     * Checks if log file is valid
     * @return boolean 
     */
    public static function validateErrorLogfile() {
        if(file_exists(self::$m_Configuration['logfile']) &&
           is_writable(self::$m_Configuration['logfile'])) {
            self::$m_LogFile = self::$m_Configuration['logfile'];
            return true;
        } else {
            self::halt('Error: Kernel died.');
            return false;
        }
    }
    
    /**
     * Clears log file
     * @return void
     */
    public static function clearLogFile() {
        if(self::$m_ValidLog) {
            $handle = fopen(self::$m_LogFile, 'w');
            fwrite($handle, '');
            fclose($handle);
        }
    }
    
    /**
     * Returns true if there are errors in the log file
     * @return boolean 
     */
    public static function errorsInLog() {
        if(filesize(self::$m_LogFile) > 0)
            return true;
        else
            return false;
    }
    
    /**
     * Halts the system
     * @param string $msg 
     */
    public static function halt($msg) {
        if(file_exists(self::$m_Configuration['halt_template'])) {
            $content = file_get_contents(self::$m_Configuration['halt_template']);
            $content = str_replace('%title%', 'Error', $content);
            $content = str_replace('%msg%', $msg, $content);
                
            exit($content);
        } else exit('Kernel died.');
    }
    
    /**
     * Development error handler
     * @param int $errNo
     * @param string $errMsg
     * @param string $errFile
     * @param string $errLine 
     */
    public static function devErrorHandler($errNo, $errMsg, $errFile, $errLine) {
        $err = '<b>Error</b> ('.$errNo.'): '.$errMsg.' in '.$errFile.' on line';
        $err .= ' '.$errLine;
        
        $log = str_replace('<b>Error</b> ', '', $err);
        self::trigger('SRC', $log, PRIO_L);
        exit($err);
    }
    
    /**
     * Live error handler
     * @param int $errNo
     * @param string $errMsg
     * @param string $errFile
     * @param string $errLine 
     */
    public static function liveErrorHandler($errNo, $errMsg, $errFile, $errLine) {
        
    }
    
    /**
     * Triggers an error
     * @param string $section
     * @param string $message
     * @param const $priority PRIO_H or PRIO_L
     */
    public static function trigger($section, $message, $priority) {
        self::$m_ErrSection = $section;
        self::$m_ErrMsg     = $message;
        
        switch($priority) {
            case PRIO_H:
                self::writeLog(self::$m_ErrSection, self::$m_ErrMsg);
                self::halt(self::$m_ErrMsg);
                self::sendNotificationMail();
                break;
            
            case PRIO_L:
                self::writeLog(self::$m_ErrSection, self::$m_ErrMsg);
                self::sendNotificationMail();
                break;
            
            default:
                /* call recursive */
                self::trigger($section, $message, PRIO_H);
                break;
        }
    }
    
    /**
     * Sends notification mail 
     */
    private static function sendNotificationMail() {
        // TODO: write a function for this?
    }
    
    /**
     * Writes errors in log file
     * This function is called by the trigger() method and can't be called 
     * directly
     */
    private static function writeLog() {
        $handle = fopen(self::$m_LogFile, 'a+');
        
        // parse logfile string
        $content = '';
        $string = self::$m_Configuration['logfile_string'];
        $exp = explode(' ', $string);
        foreach($exp as $element) {
            list($obj, $strPad) = explode('|', $element);

            switch($obj) {
                case 'date':
                    $content .= str_pad(CURRENT_DATE, $strPad);
                    break;

                case 'ip':
                    $content .= str_pad(CLIENT_IP, $strPad);
                    break;

                case 'type':
                    $content .= str_pad(self::$m_ErrSection, $strPad);
                    break;

                case 'url':
                    $content .= str_pad(ROOT, $strPad);
                    break;

                case 'msg':
                    if(self::$m_ErrMsg == '')
                        $message = 'Unknown error (have fun searching :-))';

                    $content .= str_pad(self::$m_ErrMsg, $strPad);
                    break;
            }
        }
        $content .= "\n";

        fwrite($handle, $content);
        fclose($handle);
    }
    
    public static function truncateLogFile() {
        $handle = fopen(self::$m_LogFile, 'w');
        fclose($handle);
        
        return true;
    }
}