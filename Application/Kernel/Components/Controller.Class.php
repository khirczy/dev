<?php if(!defined('ROOT')) header('Location: ../../index.php');

/**
 * Controller
 * 
 * This class handles urls, loads content with associated classes and more.
 * 
 * @author Kevin Hirczy
 * @copyright 2012
 * @package Kernel
 * @subpackage Controller
 * @since 1.0.0
 * @version 1.0.0
 */
class Controller /* extends Cache */ {
    /**
     * Current request
     * @var string
     */
    private $m_Request = null;
    
    /**
     * Default language
     * @var string
     */
    private $m_DefaultLanguage = 'de';
    
    /**
     * Internal storage for configuration
     * @var array
     */
    private $m_Configuration;
    
    /**
     * Storage for requested module
     * @var string
     */
    public $module;
    /**
     * Storage for requested id
     * @var int
     */
    public $id;
    /**
     * Storage for requested actions
     * @var string
     */
    public $action;
    /**
     * Storage for language
     * @var string
     */
    public $lang;
    /**
     * Storage for requested page
     * @var string
     */
    public $page;
    /**
     * Storage for a modules specific settings
     * @var array
     */
    public $m_ModuleSettings;
    
    /**
     * Forces a reload of the page, adding language to url and removing items
     * when needed.
     * @param string $url
     * @param array $itemsToRemove 
     */
    public function forceReload($url = null, array $itemsToRemove = null) {
        if($url == null)
            $url = REQUEST_URI;
        
        $exp = explode('/', trim($url, '/'));
        /*$newURL = $exp[0].'/';*/
        $newURL = '';
        
        if(!isset($exp[1]) ||
           !in_array($exp[1], $this->m_Configuration['languages'])) {
            $newURL .= $this->m_DefaultLanguage.'/';
        }
        
        for($i = 1; $i <= count($exp) - 1; $i++) {
            if(!in_array($exp[$i], $itemsToRemove))
                $newURL .= $exp[$i].'/';
        }
        
        header('Location: '.$newURL);
    }
    
    /**
     * Loads module by name
     * @param string $name 
     */
    protected function loadModule($name) {
        $classFile = DIR_APP . DS . DIR_MODULES . DS . ucfirst($name) . DS .
                     ucfirst($name) . '.Class.php';
        
        if(file_exists($classFile)) {
            require $classFile;

            $className = ucfirst($name);
            if(class_exists($className)) {
                ${$className} = new $className;
                
                if(isset(${$className}::$settings))
                    $this->m_ModuleSettings = ${$className}::$settings;
            }
        }
    }
    
    /**
     * Constructor for controller
     * @param array $controllerConfiguration 
     */
    function __construct(array $controllerConfiguration) {
        $this->m_Configuration = $controllerConfiguration;
        $this->m_DefaultLanguage = $this->m_Configuration['default_language'];
        
        $this->m_Request = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
        $root = $this->m_Request[0];
        
        /* language */
        if(!isset($this->m_Request[1])) {
            $this->forceReload();
        } else {
            if(in_array($this->m_Request[1],
                        $this->m_Configuration['languages'])) {
                $this->lang = $this->m_Request[1];
            } else {
                $this->forceReload();
            }
        }
        
        /* get module, action, id and page */
        if(isset($this->m_Request[2])) {
            /* will always be module */
            $this->module = $this->m_Request[2];
            $this->loadModule($this->module);
        }
        
        if(isset($this->m_Request[3])) {
            /* possibilities:
             * id
             * action
             * page
             */
            if(is_numeric($this->m_Request[3])) {
                /* id */
                $this->id = $this->m_Request[3];
            } elseif($this->m_Request[3][0] == 'p' &&
                     is_numeric(substr($this->m_Request[3], 1))) {
                /* page */
                $this->page = substr($this->m_Request[3], 1);
            } elseif(is_string($this->m_Request[3]) &&
                     $this->m_Request[3][0] != 'p') {
                /* action */
                $this->action = $this->m_Request[3];
            }
        }
        
        if(isset($this->m_Request[4])) {
            /* possibilities
             * id
             * page
             */
            if($this->m_Request[4][0] == 'p' &&
               is_numeric(substr($this->m_Request[4], 1))) {
                /* page */
                if($this->page == '') // prevent double page numbering
                    $this->page = substr($this->m_Request[4], 1);
            } elseif(is_numeric($this->m_Request[4])) {
                /* id */
                if($this->id == '') // prevent double id
                    $this->id = $this->m_Request[4];
            }
        }
        
        if(isset($this->m_Request[5])) {
            /* will always be a page */
            if(is_numeric(substr($this->m_Request[5], 1))) {
                if($this->page == '') // prevent double (or tripple!) page numbering
                    $this->page = substr($this->m_Request[5], 1);
            }
        }
    }
}