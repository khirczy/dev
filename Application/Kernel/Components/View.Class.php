<?php
final class View extends Database implements DBData {
    const THEME_DIR = 'Theme/';
    
    private $m_Content;
    
    public function show() {
        global $view, $reg, $sys, $ctrl, $log, $db, $usr;
        
        require self::THEME_DIR.'index.php';
    }
    
    public function __construct() {
        ;
    }
    
    public function getPagetitle() {
        $pTitle = Kernel::$Registry->get('pagetitle');
        
        if($pTitle != '') {
            $ret = $pTitle;
            if(Kernel::$Controller->module != '' &&
               Kernel::$Controller->m_ModuleSettings['ptitle'] != '')
                $ret .= ' - ' . Kernel::$Controller->m_ModuleSettings['ptitle'];
            
            return $ret;
        } else {
            return 'DevCMS';
        }
    }
    
    public function showNavigation($activeClass = '') {
        parent::query("SELECT label, href, required_status
                       FROM ".DBData::TB_NAVIGATION."
                       ORDER BY `order` ASC");
        
        $ret = '';
        
        if(parent::isAffected()) {
            while($row = parent::fetchObj()) {
                if(Kernel::$User->getStatus() >= $row->required_status) {
                    $href = str_replace('%l', Kernel::$Controller->lang, $row->href);

                    $active = ($href == REQUEST_URI ? ' class="'.$activeClass.'"' : '');
                    $ret .= '<li'.$active.'><a href="'.$href.'">'.$row->label.'</a></li>'.PHP_EOL;
                }
            }
        } else {
            Error::trigger('VIEW', 'No navigation found', PRIO_L);
            
            $ret .= '<li><a href="./">Home</a></li>'.PHP_EOL;
        }
        
        return $ret;
    }
    
    public function showContent() {
        if(Kernel::$Controller->module != '') {
            $path = ROOT_DIR . DIR_APP . DS . DIR_MODULES . DS . Kernel::$Controller->m_ModuleSettings['module_dir'] . DS . 'frontend';
            $rooter = Kernel::$Controller->m_ModuleSettings['theme_frontend_rooter'];
            if(file_exists($path . DS . $rooter)) {
                require $path . DS . $rooter;
            } else {
                Error::trigger ('VIEW', 'Content could not be loaded ('.$path . DS . $rooter.')', PRIO_L);
                $e404 = ROOT_DIR . DIR_THEME . DS . 'err-404.php';
                if(file_exists($e404)) {
                    require ROOT_DIR . DIR_THEME . DS . 'err-404.php';
                } else {
                    Error::trigger('VIEW', 'Missing error 404 page', PRIO_L);
                    echo '<div align="center">File not found.</div>';
                }
            }
        } else {
            echo "home..";
        }
    }
    
    public function getSiteName() {
        return Kernel::$Registry->get('sitename');
    }
}