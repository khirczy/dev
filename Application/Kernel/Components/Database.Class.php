<?php if(!defined('ROOT')) header('Location: ../../index.php');

/**
 * Database management
 * 
 * This class handles database connection and everything related with
 * the database.
 * 
 * @author Kevin Hirczy
 * @copyright 2012
 * @package Kernel
 * @subpackage Database
 * @since 1.0.0
 * @version 1.0.0
 */
class Database {
    /**
     * Database hostname
     * @var string
     */
    private $m_Host;
    /**
     * Database user
     * @var string
     */
    private $m_User;
    /**
     * Database password
     * @var string
     */
    private $m_Pass;
    /**
     * Database name
     * @var string
     */
    private $m_Database;
    
    /**
     * Internal link for current connection
     * @var resource
     */
    private $m_Connection;
    
    /**
     * Connection is up or not
     * @var boolean
     */
    public $m_Connected;
    
    /**
     * Enables query counter
     * @var boolean
     */
    private $m_Affected = 0; /** Counter for affected rows by a query @var int */
    
    /**
     * Stores error
     * @var string
     */
    public $m_Error;
    
    /**
     * Internal storage for current query
     * @var resource
     */
    private $m_Query;
    /**
     * Storage for last query
     * @var resource
     */
    private $m_LastQuery;
    
    /**
     * Returns errors
     * @return string
     */
    public function panic() {
        return $this->m_Error;
    }
    
    /**
     * Selects the database
     * @return boolean 
     */
    private function selectDatabase() {
        if(mysql_select_db($this->m_Database))
            return true;
        else
            return false;
    }
    
    /**
     * Method to insert data into the database
     * @example insert('users', array('name' => $name, 'password' => $pwd));
     * @param string $table Tablename
     * @param array $data Data to be changed
     * @return void 
     */
    public function insert($table, array $data) {
        $string = 'INSERT INTO `'.$table.'` ';
        $keys   = '';
        $vals   = '';
        
        foreach($data as $key => $value) {
            $keys .= '`'.$key.'`, ';
            $vals .= '\''.$this->escape($value).'\', ';
        }
        
        $string .= '('.rtrim($keys, ', ').') VALUES
                    ('.rtrim($vals, ', ').')';
        
        $this->query($string);
        return true;
    }
    
    /**
     * Method to update existing data
     * @example update('users', array('name' => $newName), 'id' => 15));
     * @param string $table
     * @param array $data
     * @param array $conditions
     * @return void
     */
    public function update($table, array $data, array $conditions = null) {
        $string = 'UPDATE `'.$table.'` SET ';
        
        foreach($data as $key => $value) {
            $string .= $key.' = "'.$this->escape($value).'", ';
        }
        
        if($conditions != null) {
            $where = 'WHERE ';
            $elements = count($conditions);
            $i = 1;
            
            foreach($conditions as $column => $requirement) {
                $where .= $column.'= "'.$this->escape($requirement).'"';
                if($i != $elements)
                    $where .= ' AND ';
                $i++;
            }
        }
        
        $string = rtrim($string, ', ').(isset($where) ? $where : '');
        $this->query($string);
        return true; // TODO: What to return? (update)
    }
    
    /**
     * Deletes some data
     * @example delete('users', array('id' => 2));
     * @param string $table
     * @param array $conditions
     * @return void 
     */
    public function delete($table, array $conditions = null) {
        $string = 'DELETE FROM `'.$table.'` ';
        if($conditions != null) {
            $string .= 'WHERE ';
            $elements = count($conditions);
            $i = 1;
            
            foreach($conditions as $column => $requirement) {
                $string .= $column.'= "'.$this->escape($requirement).'"';
                if($i != $elements)
                    $string .= ' AND ';
                $i++;
            }
        }
        
        $this->query($string);
        return true;
    }
    
    /**
     * Creates an object of a query
     * @param resource $query
     * @return object
     */
    public function fetchObj($query = null) {
        if($query != null)
            return mysql_fetch_object($query);
        else
            return mysql_fetch_object($this->m_Query);
    }
    
    /**
     * Creates an array of a query
     * @param resource $query
     * @return array
     */
    public function fetchArr($query = null) {
        if($query != null)
            return mysql_fetch_array($query);
        else
            return mysql_fetch_array($this->m_Query);
    }
    
    /**
     * Returns affected rows by a query
     * @return int
     */
    public function getAffectedRows() {
        return mysql_affected_rows();
    }
    
    /**
     * Returns if a row is affected
     * @return boolean 
     */
    public function isAffected() {
        if($this->m_Affected > 0)
            return true;
        else
            return false;
    }
    
    /**
     * Method for mysql_query
     * @param string $string
     * @return void
     */
    public function query($string) {
        if(!$query = mysql_query($string)) {
            $this->m_Error = mysql_error();
            Error::trigger('DB', $this->m_Error, PRIO_H);
            return false;
        }
        
        $this->m_Query = $query;
        $this->m_Affected = $this->getAffectedRows();
        
        return true;
    }
    
    /**
     * Escapes a string
     * @param string $string
     * @return string
     */
    public function escape($string) {
        return mysql_real_escape_string($string);
    }
    
    /**
     * Connects to the database
     * @param string $host
     * @param string $user
     * @param string $pass
     * @param string $database 
     */
    public function connect($host = null, $user = null, $pass = null,
                            $database = null) {
        if(!$this->m_Connected) { 
            $this->m_Host = $host;
            $this->m_User = $user;
            $this->m_Pass = $pass;
            $this->m_Database = $database;

            $this->m_Error      = null;
            $this->m_LastQuery  = null;

            $this->m_Connected  = false;

            $this->m_Connection = mysql_connect(
                $this->m_Host,
                $this->m_User,
                $this->m_Pass
            );

            if($this->m_Connection) {
                if($this->selectDatabase()) {
                    $this->m_Connected = true;
                } else {
                    $this->m_Error = 'Selecting database failed';
                    $this->m_Connected = false;
                }
            } else {
                $this->m_Error = 'Connection to database failed';
                $this->m_Connected = false;
            }
        }
    }
    
    /**
     * Counts all matching rows in a table
     * @param type $table
     * @param array $condition
     * @return int
     */
    public function countRows($table, array $conditions = null) {
        $string = "SELECT COUNT(id) AS count
                   FROM ".$table;
        
        if($conditions != null) {
            $string .= ' WHERE ';
            $elements = count($conditions);
            $i = 1;
            
            foreach($conditions as $column => $requirement) {
                $string .= $column.'= "'.$this->escape($requirement).'"';
                if($i != $elements)
                    $string .= ' AND ';
                $i++;
            }
        }
        $this->query($string);
        $row = $this->fetchObj();
        return $row->count;
    }
    
    /**
     * Destructor 
     */
    public function __destruct() {
        if($this->m_Connected === true) {
            mysql_close($this->m_Connection);
            $this->m_Connected = false;
            $this->m_Connection = null;
        }
    }
}