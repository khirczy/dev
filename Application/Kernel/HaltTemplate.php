<html>
    <head>
        <title>Corrupted Kernel</title>
        <style>
            body {
                margin: 0;
                padding: 0;
                font-family: Verdana;
                font-size: 9pt;
            }
            
            div#main {
                width: 500px;
                margin: auto;
                margin-top: 60px;
            }
            
            div#main span {
                width: 100%;
                display: block;
                padding: 4px;
                background-color: #ff0000;
                color: #000;
                font-weight: bold;
            }
            
            div#main p {
                
            }
        </style>
    </head>
    
    <body>
        <div id="main">
            <span>%title%</span>
            <p>%msg%</p>
        </div>
    </body>
</html>