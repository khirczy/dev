<?php if(!defined('ROOT')) header('Location: ../../index.php');

/**
 * Common constants
 * @include Constants.php
 */
require DIR_CONFIG . DS . 'Constants.php';

/**
 * Kernel
 * 
 * Core of every project.
 * 
 * @author Kevin Hirczy
 * @copyright 2012
 * @package Kernel
 * @since 1.0.0
 * @version 1.0.0
 */
final class Kernel {
    /**
     * Enables kernel loading
     * @access private
     * @var boolean
     */
    private $m_loadKernel = true;
    
    /**
     * Required interfaces
     * @access private
     * @var array
     */
    private $m_Interfaces = array(
        'ConfigDependency',
        'DBData'
    );
    
    /**
     * Array of required components
     * @access private
     * @var array
     */
    private $m_Components = array(
        'Log' => true,
        'Registry' => true,
        'System' => true,
        'User' => true,
        'Cache' => false // abstract
    );
    
    /**
     * Class-internal storage for configuration
     * @access private
     * @var array
     */
    private $m_Config;
    
    /**
     * Records kernel corruption
     * @var bool
     */
    private $m_KernelCorruption = false;
    /**
     * Records kernel corruption reasons
     * @var array
     */
    private $m_KernelCorruptionReasons = array();
    
    /** Database object */
    public static $Database;
    /** Log object */
    public static $Log;
    /** System object */
    public static $System;
    /** User object */
    public static $User;
    /** Registry object */
    public static $Registry;
    /** Controller object */
    public static $Controller;
    /** View object */
    public static $View;
    
    /**
     * Start time of execution
     * @access public
     * @var int
     */
    public static $scriptExecutionStart;
    
    /** Package version */
    private static $m_PackageVersion;
    
    /** Kernel version */
    const KERNEL_VERSION = '1.0.0';
    
    /**
     * Returns package version
     * @param string $return Version, Name or Release
     * @access public
     * @return string
     */
    public static function getPackageVersion($return = '') {
        switch($return) {
            case 'version':
                return self::$m_PackageVersion['major'].'.'.
                       self::$m_PackageVersion['minor'].'.'.
                       self::$m_PackageVersion['patch'];
                break;
            
            case 'name':
                return self::$m_PackageVersion['name'];
                break;
            
            case 'release':
                switch(self::$m_PackageVersion['release']) {
                    case 0: return 'Stable';            break;
                    case 1: return 'Beta';              break;
                    case 2: return 'Alpha';             break;
                }
                
                break;
            
            default:
                return 0;
        }
    }
    
    private static function setPackageVersion(array $versionConfig) {
        self::$m_PackageVersion = $versionConfig;
    }
    
    /**
     * Returns kernel version
     * @access public
     * @return string
     */
    public static function getKernelVersion() {
        return self::KERNEL_VERSION;
    }
    
    /**
     * Records kernel error
     * @access private
     * @param string $reason Reason what went wrong
     */
    private function kernelCorrupted($reason) {
        $this->m_KernelCorruption = true;
        $this->m_KernelCorruptionReasons[] = $reason;
    }
    
    /**
     * Autoloader for library
     */
    private function libraryAutoloader() {
        spl_autoload_register(function ($class) {
            $path = DIR_APP . DS . DIR_LIBRARY . DS . ucfirst($class) . '.Lib.php';
            if(file_exists($path)) {
                require_once $path;
            } else {
                Error::trigger('LIB', 'Autoloader failed to include "'.$class.'" library.', PRIO_H);
            }
        });
    }
    
    /**
     * Sets time when the script is started
     * @access private
     */
    private function setScriptExecutionStart() {
        $t = microtime();
        $t = explode(' ', $t);
        self::$scriptExecutionStart = $t[1] + $t[0];
    }
    
    /**
     * Loads configuration
     * @access private
     */
    private function loadConfigurationFile() {
        $loadConfig = true;
        $path = DIR_APP . DS . DIR_KERNEL . DS . DIR_CONFIG . DS . 'Config.php';
        
        if(file_exists($path))
            $this->m_Config = require $path;
        else
            $this->kernelCorrupted('Missing config file');
        
        unset($loadConfig);
    }
    
    /**
     * Basic configuration (execution limit, memory limit, default timezone)
     * @access private
     */
    private function setDefaultConfiguration() {
        set_time_limit($this->m_Config['global']['execution_limit']);
        ini_set('memory_limit', $this->m_Config['global']['memory_limit']);
        ini_set('max_execution_time', $this->m_Config['global']['execution_limit']);
        date_default_timezone_set($this->m_Config['global']['default_timezone']);
    }
    
    /**
     * Checks if the installed interpreter is ready for this software
     * @access private
     */
    private function validateInterpreter() {
        if(phpversion() < $this->m_Config['global']['required_php_version'])
            $this->kernelCorrupted('Invalid Interpreter version');
    }
    
    /**
     * Includes interfaces
     * @access private
     */
    private function includeInterfaces() {
        foreach($this->m_Interfaces as $interface) {
            $path = DIR_APP . DS . DIR_KERNEL . DS .
                    DIR_INTERFACES . DS . $interface . '.Interface.php';
            
            if(file_exists($path))
                require $path;
            else
                $this->kernelCorrupted ('Missing interface ('.$interface.')');
        }
    }
    
    /**
     * Handles error management
     * @access private
     */
    private function setErrorManagement() {
        require DIR_COMPONENTS . DS . 'Error.Class.php';
        Error::setConfiguration($this->m_Config['error']);
        Error::validateErrorLogFile();

        if($this->m_Config['global']['environment'] == ENV_DEVELOPMENT) {
            ini_set('display_errors', 'on');
            ini_set('error_reporting', E_ALL);
            error_reporting(-1);
            set_error_handler('Error::devErrorHandler');
        } elseif($this->m_Config['global']['environment'] == ENV_RELEASE) {
            ini_set('display_errors', 'off');
            ini_set('error_reporting', 0);
            error_reporting(0);
            set_error_handler('Error::liveErrorHandler');
        } else
            $this->kernelCorrupted('Invalid environment');
    }
    
    /**
     * Handles database management
     * @access private
     */
    private function setDatabase() {
        $loadDbAccess = true;
        
        $dbAccess = require_once DIR_CONFIG . DS . 'DatabaseAccess.php';
        require DIR_COMPONENTS . DS . 'Database.Class.php';
        
        self::$Database = new Database;
        self::$Database->connect($dbAccess['host'], $dbAccess['user'],
                                 $dbAccess['pass'], $dbAccess['database']);
        
        if(!self::$Database->m_Connected)
            $this->kernelCorrupted(self::$Database->panic());
        
        unset($loadDbAccess);
    }
    
    /**
     * Checks if there where any kernel corruptions
     * @return boolean 
     */
    private function getKernelCorruption() {
        if($this->m_KernelCorruption)
            return true;
        else
            return false;
    }
    
    /**
     * Include components
     * @access private
     */
    private function initComponents() {
        foreach($this->m_Components as $component => $createObj) {
            $componentPath = DIR_APP . DS .
                             DIR_KERNEL . DS . DIR_COMPONENTS . DS .
                             $component . '.Class.php';
            
            if(file_exists($componentPath)) {
                require $componentPath;
                if($createObj)
                    self::${$component} = new $component;
            } else {
                $failedComp = $componentPath.' ('.$component.')';
                $this->kernelCorrupted('Failed to init '.$failedComp);
            }
        }
    }
    
    /**
     * Initializes controller
     * @access private
     */
    private function initController() {
        $path = DIR_APP . DS . DIR_KERNEL . DS .
                DIR_COMPONENTS .DS . 'Controller.Class.php';
        if(file_exists($path)) {
            require $path;
            self::$Controller = new Controller($this->m_Config['controller']);
        } else {
            $this->kernelCorrupted('Failed to init Controller');
        }
    }
    
    /**
     * Initializes view
     * @access private
     */
    private function initView() {
        $path = DIR_APP . DS . DIR_KERNEL . DS .
                DIR_COMPONENTS .DS . 'View.Class.php';
        if(file_exists($path)) {
            require $path;
            self::$View = new View();
        } else {
            $this->kernelCorrupted('Failed to init View');
        }
    }
    
    public function getRootPassword() {
        
    }
    
    /**
     * Initializes kernel methods
     * @access public
     * @return void 
     */
    function __construct($initController = true) {
        $this->setScriptExecutionStart();
        
        $this->loadConfigurationFile();
        $this->setDefaultConfiguration();
        $this->validateInterpreter();
        $this->includeInterfaces();
        $this->setErrorManagement();
        $this->setDatabase();
        
        $this->libraryAutoloader();
        
        $this->initComponents();
        
        /** modification for administration */
        if($initController)
            $this->initController();
        
        $this->initView();
        
        if($this->getKernelCorruption()) {
            foreach($this->m_KernelCorruptionReasons as $reason) {
                Error::trigger('KRNL', $reason, PRIO_L);
            }
            
            Error::halt('Kernel corrupted. See log file for further
                         information.', null, false);
        }
        
        /** set package version */
        self::setPackageVersion($this->m_Config['version']);
        
        /** Disable kernel loading */
        $this->m_loadKernel = false;
    }
}