<?php
interface DBData {
    const TB_LOG            = 'log';
    const TB_REGISTRY       = 'registry';
    const TB_USERS          = 'users';
    const TB_NAVIGATION     = 'navigation';
    const TB_FILEVALIDATION = 'file_validation';
    const TB_BACKUPS        = 'backups';
}