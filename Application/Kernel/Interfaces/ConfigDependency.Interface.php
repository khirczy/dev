<?php
/**
 * Interface for config dependency
 * 
 * @package Configuration
 * @since 1.0.0
 */

interface ConfigDependency {
    public static function setConfiguration(array $config);
}