<?php
class Test {
    public static $settings = array(
        'name' => 'Test',
        'ptitle' => 'Test',
        
        'frontend_navigation' => array(
            'Test' => 'test'
        ),
        
        'module_dir' => 'Test',
        
        'theme_frontend_rooter' => 'test.php',
        'theme_backend_rooter' => 'test.php',
        
        'version' => '1.0.0',
        'author' => 'Hirczy Kevin'
    );
    
    function __construct() {
        
    }
}