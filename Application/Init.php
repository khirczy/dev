<?php if(!defined('ROOT')) header('Location: ../index.php');
/**
 * Initialize the application
 * 
 * @author Kevin Hirczy
 * @copyright 2012
 * @package Application
 * @since 1.0.0
 * @version 1.0.0
 */

define('DS', DIRECTORY_SEPARATOR);

define('DIR_APP', 'Application');

define('DIR_KERNEL', 'Kernel');
define('DIR_CONFIG', 'Config');
define('DIR_INTERFACES', 'Interfaces');
define('DIR_COMPONENTS', 'Components');
define('DIR_CACHE', 'Cache');
define('DIR_LANGUAGE', 'Language');
define('DIR_LIBRARY', 'Library');
define('DIR_MODULES', 'Modules');
define('DIR_THEME', 'Theme');

require DIR_KERNEL . DS . 'Kernel.php';
    new Kernel();
    
/* aliases */
$reg    = Kernel::$Registry;
$sys    = Kernel::$System;
$ctrl   = Kernel::$Controller;
$log    = Kernel::$Log;
$db     = Kernel::$Database;
$usr    = Kernel::$User;
$view   = Kernel::$View;