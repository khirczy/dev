<?php
ob_start();

define('ROOT', realpath(__FILE__));
define('ROOT_DIR', str_replace('index.php', '', ROOT));

require 'Application/Init.php';

$view->show();

include 'getExecTime.php';
require 'Application/Footer.php';