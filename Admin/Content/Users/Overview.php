<div class="row">
    <div class="span4">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="2">Legende</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td width="10%"><span class="badge badge-success"></span></td>
                    <td width="90%">Benutzer aktiviert und nicht gesperrt</td>
                </tr>
                <tr>
                    <td><span class="badge badge-warning"></span></td>
                    <td>Benutzer noch nicht aktiviert</td>
                </tr>
                <tr>
                    <td><span class="badge badge-important"></span></td>
                    <td>Benutzer gesperrt</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="span8" align="right">
        <a class="btn btn-info" href="index.php?c=users&amp;a=add"><i class="icon-user icon-white"></i> Neuen Benutzer anlegen</a>
    </div>
</div>

<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th width="4%"><div align="center">#</div></td>
            <th width="4%"></td>
            <th width="27%">Benutzername</td>
            <th width="27%">Anzeigename</td>
            <th width="16%">Rolle</td>
            <th width="11%"></td>
            <th width="11%"></td>
        </tr>
    </thead>
    <tbody>
        <?php
        Pagination::setTotalItems($db->countRows(DBData::TB_USERS));
        Pagination::setItemsPerPage($reg->get('users_per_page'));
        
        $db->query("SELECT id, name, display_name, status
                    FROM ".DBData::TB_USERS."
                    ORDER BY name ASC
                    LIMIT ".Pagination::SQLLimits());
        while($row = $db->fetchObj()) {
        ?>
        <tr>
            <td><div align="center"><?= $row->id; ?></div></td>
            <td>
                <div align="center">
                    <?php
                    switch($row->status) {
                        case ($row->status == USR_BANNED):
                            $badge = 'badge-success';
                            break;
                        
                        case ($row->status == USR_NOTACTIVATED):
                            $badge = 'badge-warning';
                            break;
                        
                        default:
                            $badge = 'badge-success';
                            break;
                    }
                    ?>
                    <span class="badge <?= $badge ;?>"></span>
                </div>
            </td>
            <td><?= $row->name; ?></td>
            <td><?= $row->display_name; ?></td>
            <td><?= $adm->userStatusLabel($row->id); ?></td>
            <td><div align="center"><a class="btn btn-primary btn-mini" href="index.php?c=users&amp;a=edit&amp;id=<?= $row->id; ?>"><i class="icon-edit icon-white"></i> Bearbeiten</a></div></td>
            <td>
                <?php
                if(($row->id == 1 && $usr->isRoot()) || $row->id != 1) {
                ?>
                <div align="center"><a class="btn btn-danger btn-mini" href="index.php?c=users&amp;a=del&amp;id=<?= $row->id; ?>" onclick="return confirm('Sind Sie sicher, dass Sie diesen Benutzer löschen wollen?');"><i class="icon-trash icon-white"></i> Löschen</a></div>
                <?php
                }
                ?>
            </td>
        </tr>
        <?php
        }
        ?>
    </tbody>
</table>

<div class="pagination" align="center">
    <ul>
        <?= Pagination::pageNavigation(); ?>
    </ul>
</div>