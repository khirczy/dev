<?php
if($sys->catchPostEvent('add_user')) {
    $loginName      = $sys->getPostValue('add_user_name');
    $displayName    = $sys->getPostValue('add_user_display_name');
    $mail           = $sys->getPostValue('add_user_mail');
    $password       = $sys->getPostValue('add_user_password');
    $role           = $sys->getPostValue('add_user_role');
    
    if($loginName != '' &&
       $displayName != '' &&
       $mail != '' &&
       $password != '') {
        
    } else {
        
    }
}
?>

<form class="form-horizontal">
    <legend>Neuen Benutzer anlegen</legend>
    <div class="control-group">
        <label class="control-label" for="add_user_name">Login-Name</label>
        <div class="controls">
            <input type="text" id="add_user_name" class="input-small" />
            <span class="help-block">Mit diesem Namen kann sich der Benutzer einloggen</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="add_user_display_name">Anzeigename</label>
        <div class="controls">
            <input type="text" id="add_user_display_name" class="input-medium" />
            <span class="help-block">Dieser Name wird stellvertretend für den Benutzer angezeigt</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="add_user_mail">E-Mail</label>
        <div class="controls">
            <input type="text" id="add_user_mail" class="input-medium" />
            <span class="help-block">Die E-Mail-Adresse des Benutzers</span>
        </div>
    </div>
    <br />
    <div class="control-group">
        <label class="control-label" for="add_user_mail">Passwort</label>
        <div class="controls">
            <input type="text" id="add_user_mail" class="input-large">
            <span class="help-block">Mindestens 4 Zeichen lang</span>
        </div>
    </div>
    <br />
    <div class="control-group">
        <label class="control-label" for="add_user_mail">Rolle</label>
        <div class="controls">
            <select class="input-medium">
                <option>Benutzer</option>
                <option>Moderator</option>
                <option>Administrator</option>
            </select>
            <span class="help-block">Die Rolle des Benutzers</span>
        </div>
    </div>
    <br />
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-primary" name="add_user"><i class="icon-plus icon-white"></i> Benutzer anlegen</button>
            <a href="index.php?c=users" class="btn">Abbrechen</a>
        </div>
    </div>
</form>