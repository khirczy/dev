<?php
$validation = array(
    'index.php',
    '.htaccess',
    'Admin',
    'Admin/Content',
    'Admin/Content/SystemControl',
    'Admin/Content/SystemControl/Registry',
    'Admin/Content/SystemControl/UserLog',
    'Admin/Include',
    'Admin/Layout',
    'Application',
    'Application/Kernel',
    'Application/Kernel/Components',
    'Application/Kernel/Config',
    'Application/Kernel/Interfaces',
    'Application/Library',
    'Application/Modules',
    'Theme',
);

function updateSizeToDatabase($file, $size) {
    global $db;
    
    $db->query("SELECT file
                FROM ".DBData::TB_FILEVALIDATION."
                WHERE file = '".$file."'");
    
    if($db->isAffected()) {
        /** update */
        $db->update(DBData::TB_FILEVALIDATION, array(
            'size' => $size
        ), array('file' => $file));
    } else {
  /** insert */
        $db->insert(DBData::TB_FILEVALIDATION, array(
            'file' => $file,
            'size' => $size
        ));
    }
}

if($sys->catchGetVar('save', 'true')) {
    foreach($validation as $item) {
        if(is_dir('../'.$item)) {
            foreach(glob('../'.$item.'/*') as $file) {
                if(!is_dir($file)) {
                    $filesize = filesize($file);
                    updateSizeToDatabase($file, $filesize);
                }
            }
        } else { /** item is file */
            $filesize = filesize('../'.$item);
            updateSizeToDatabase($item, $filesize);
        }
    }
    
    $log->record('SC', 'Saved current state of files', $usr->getID());
    FormSubmission::submit('file_validation', FRM_SUBM_SUCCESS, 'Done.', 'index.php?c=sc&a=filevalidation');
}

function compareToDatabase($file, $currentSize) {
    global $db;
    
    $db->query("SELECT file, size
                FROM ".DBData::TB_FILEVALIDATION."
                WHERE file = '".$file."'");
    
    if($db->isAffected()) {
        $row = $db->fetchObj();
        
        return $row->size;
    } else
        return 0;
}

FormSubmission::showContainer('validate_files');
?>
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th width="60%">File</th>
            <th width="20%">Current size</th>
            <th width="20%">Original size</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $totalSize = 0;
        $totalSizeDB = 0;
        
        foreach($validation as $item) {
            if(is_dir('../'.$item)) {
                echo '<tr>
                    <td colspan="3"><i>'.$item.'</i></td>
                </tr>';
                
                foreach(glob('../'.$item.'/*') as $file) {
                    if(!is_dir($file)) {
                        $filesize = filesize($file);
                        $filesizeDB = compareToDatabase($file, $filesize);
                        
                        $totalSize += $filesize;
                        $totalSizeDB += $filesizeDB;
                        $prettyFileName = str_replace('../'.$item.'/', '', $file);
                        
                        if($filesize != $filesizeDB) {
                            $trClass = ' class="error"';
                        } else {
                            $trClass = '';
                        }
                        
                        echo '<tr'.$trClass.'>
                            <td style="padding-left: 40px;">'.$prettyFileName.'</td>
                            <td>'.round($filesize / 1024, 2).' KBytes</td>
                            <td>'.round(compareToDatabase($file, filesize($file)) / 1024, 2).' KBytes</td>
                        </tr>';
                    }
                }
            } else { /** item is file */
                $filesize = filesize('../'.$item);
                $filesizeDB = compareToDatabase($item, filesize('../'.$item));
                
                $totalSize += $filesize;
                $totalSizeDB += $filesizeDB;
                
                if($filesize != $filesizeDB) {
                    $trClass = ' class="error"';
                } else {
                    $trClass = '';
                }
                
                echo '<tr'.$trClass.'>
                    <td>'.$item.'</td>
                    <td>'.round($filesize / 1024, 2).' KBytes</td>
                    <td>'.round(compareToDatabase($item, filesize('../'.$item)) / 1024, 2).' KBytes</td>
                </tr>';
            }
        }
        ?>
        <tr>
            <td><b>Total</b></td>
            <td><?= round($totalSize / 1024 / 1024, 2); ?> MBytes</td>
            <td><?= round($totalSizeDB / 1024 / 1024, 2); ?> MBytes</td>
        </tr>
    </tbody>
</table>

<div align="right">
    <a class="btn btn-primary" href="index.php?c=sc&amp;a=filevalidation&amp;save=true" onclick="return confirm('Sure?');"><i class="icon-download icon-white"></i> Save current state</a>
</div>