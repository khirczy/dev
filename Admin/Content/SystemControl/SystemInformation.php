<h4>Application</h4>
<table class="table table-hover">
    <tr>
        <td width="50%">Version</td>
        <td width="50%"><?= Kernel::getPackageVersion('version'); ?></td>
    </tr>
    <tr>
        <td>Name</td>
        <td><?= Kernel::getPackageVersion('name'); ?></td>
    </tr>
    <tr>
        <td>Release</td>
        <td><?= Kernel::getPackageVersion('release'); ?></td>
    </tr>
    <tr>
        <td>Kernel</td>
        <td><?= Kernel::getKernelVersion(); ?></td>
    </tr>
    <tr>
        <td>Administration</td>
        <td><?= ADMIN_VERSION; ?></td>
    </tr>
</table>

<h4>Modules</h4>
<table class="table table-hover">
    <tr>
        <td>...</td>
    </tr>
</table>

<h4>Server</h4>
<table class="table table-hover">
    <tr>
        <td width="50%">PHP</td>
        <td width="50%"><?= phpversion(); ?></td>
    </tr>
    <tr>
        <td>Software</td>
        <td><?= $_SERVER['SERVER_SOFTWARE']; ?></td>
    </tr>
    <tr>
        <td>IP</td>
        <td><?= $_SERVER['SERVER_ADDR']; ?></td>
    </tr>
</table>

<h4>mySQL Database</h4>
<?php
$db->query("SHOW TABLE STATUS");
?>
<table class="table table-hover">
    <tbody>
        <?php
        $row = $db->fetchArr();
        ?>
        <tr>
            <td width="50%">Version</td>
            <td width="50%"><?= mysql_get_server_info(); ?></td>
        </tr>
        <tr>
            <td>Engine</td>
            <td><?= $row['Engine']; ?></td>
        </tr>
    </tbody>
</table>

<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th width="60%">Table</td>
            <th width="20%">Entries</td>
            <th width="20%">Size</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $totalRows = 0;
        $totalSize = 0;
        
        while($row = $db->fetchArr()) {
            $size = ($row['Data_length'] +
                     $row['Index_length']) / 1024;
            
            $totalRows += $row['Rows'];
            $totalSize += $size;
            
        ?>
        <tr>
            <td><?= $row[0]; ?></td>
            <td><?= $row["Rows"]; ?></td>
            <td><?= round($size, 2); ?> KBytes</td>
        </tr>
        <?php
        }
        ?>
        <tr>
            <td><b>Total</b></td>
            <td><?= $totalRows; ?></td>
            <td><?= round($totalSize, 2); ?> KBytes</td>
        </tr>
    </tbody>
</table>