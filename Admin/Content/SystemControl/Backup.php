<?php
$lastBackup = $reg->get('last_backup');
if($lastBackup == '') {
    $lastBackup = 'never';
} else {
    $lastBackup = date('d.m.Y H:i:s', $lastBackup);
}
?>
<table class="table table-hover">
    <tr>
        <td width="50%">Last backup</td>
        <td width="50%"><?= $lastBackup; ?></td>
    </tr>
</table>

<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th>Backups</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $db->query("SELECT time, user
                    FROM ".DBData::TB_BACKUPS."
                    ORDER BY id DESC");
        while($row = $db->fetchObj()) {
        ?>
        <tr>
            <td width="90%">backup-<?=date('dmY-His', $row->time);?>-<?=$row->user;?></td>
            <td width="10%"></td>
        </tr>
        <?php
        }
        ?>
    </tbody>
</table>

<div align="right">
    <a class="btn btn-primary" href="../Application/Backup/ExecuteBackup.phpx?uid=<?= $usr->getID(); ?>" onclick="return confirm('Sure?');"><i class="icon-download icon-white"></i> Backup database</a>
</div>