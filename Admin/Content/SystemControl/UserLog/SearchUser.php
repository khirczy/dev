<?php
if($sys->catchPostEvent('search_user')) {
    $user = trim($_POST['search_user_nameid']);
    
    if($user != '') {
        $where = (is_numeric($user) ? "id = '".$user."'" : "name = '".$user."'");
        
        $db->query("SELECT id, name
                    FROM ".DBData::TB_USERS."
                    WHERE ".$where);
        
        if($db->isAffected() || $user == USR_ROOT_NAME) {
            if($user != USR_ROOT_NAME) {
                $row = $db->fetchObj();
                $user = $row->id;
            } else {
                $user = USR_ROOT_ID;
            }
        } else $user = (USR_ROOT_ID - 1);
    } else $user = (USR_ROOT_ID - 1);
    
    header("Location: index.php?c=sc&a=ulog&uid=".$user);
}
?>

<form class="well form-inline" action="index.php?c=sc&amp;a=ulog" method="post" name="ulog_search">
    <div class="input-prepend">
        <span class="add-on">Name / ID:</span><input class="span2" id="search_user_nameid" name="search_user_nameid" type="text" />
    </div>
    <button type="submit" class="btn" name="search_user">Search user</button>
</form>