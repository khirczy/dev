<?php
require 'UserLog/SearchUser.php';

if($sys->catchGetVar('uid')) {
    Pagination::setTotalItems($db->countRows(DBData::TB_LOG,
                                             array('uid' => $sys->catchGetVar('uid'))
    ));
    Pagination::setItemsPerPage($reg->get('ulog_items_per_page'));
    
    $db->query("SELECT uid, time, action, section
                FROM ".DBData::TB_LOG."
                WHERE uid = '".$db->escape($sys->catchGetVar('uid'))."'
                ORDER BY time DESC
                LIMIT ".Pagination::SQLLimits());
    
    if($db->isAffected()) {
    ?>
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th width="60%">Action</td>
                <th width="26%">Date</td>
                <th width="14%">Section</td>
            </tr>
        </thead>
        <tbody>
        <?php
        while($row = $db->fetchObj()) {
        ?>
            <tr>
                <td><?= $row->action; ?></td>
                <td><?= date('d.m.Y H:i:s', $row->time); ?></td>
                <td><?= $row->section; ?></td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>

    <div class="pagination" align="center">
        <ul>
            <?= Pagination::pageNavigation(); ?>
        </ul>
    </div>
    <?php
    } else {
        echo '<div align="center">Nothing found</div>';
    }
}