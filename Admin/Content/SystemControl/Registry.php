<?php
FormSubmission::showContainer('registry');

if($sys->catchGetVar('edit'))       require 'Registry/EditItem.php';
elseif($sys->catchGetVar('del'))    require 'Registry/DelItem.php';
else                                require 'Registry/NewItem.php';
?>

<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th width="36%">Name</td>
            <th width="50%">Value</td>
            <th width="7%"></td>
            <th width="7%"></td>
        </tr>
    </thead>
    <tbody>
        <?php
        $db->query("SELECT *
                    FROM ".DBData::TB_REGISTRY."
                    ORDER BY name ASC");
        
        while($row = $db->fetchObj()) {
            if(($row->locked == 1 && $usr->isRoot()) || $row->locked == 0):
        ?>
        <tr>
            <td width="36%"><?= ($row->locked == 1) ? '<span class="label label-important">'.$row->name.'</span>' : $row->name; ?></td>
            <td width="50%"><?= $row->value; ?></td>
            <td width="7%"><a class="btn btn-primary btn-mini" href="index.php?c=sc&amp;a=registry&edit=<?=$row->id;?>"><i class="icon-pencil icon-white"></i></a></td>
            <td width="7%"><a class="btn btn-danger btn-mini" href="index.php?c=sc&amp;a=registry&del=<?=$row->id;?>" onclick="return confirm('Sure?');"><i class="icon-trash icon-white"></i></a></td>
        </tr>
        <?php
            endif;
        }
        ?>
    </tbody>
</table>