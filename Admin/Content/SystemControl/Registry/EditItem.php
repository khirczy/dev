<?php
if($sys->catchGetVar('edit')) {
    $db->query("SELECT name, value, locked
                FROM ".DBData::TB_REGISTRY."
                WHERE id = '".$sys->catchGetVar('edit')."'");
    
    if($db->isAffected()) {
        $row = $db->fetchObj();
        
        if($row->locked == 1 && !$usr->isRoot()) {
            header("Location: index.php?c=sc&a=registry");
        }
    } else {
        header("Location: index.php?c=sc&a=registry");
    }
}

if($sys->catchPostEvent('reg_item_edit')) {
    $prevName = $row->name;
    
    $name = $sys->getPostValue('reg_item_name');
    $value = $sys->getPostValue('reg_item_value');
    
    $db->update(DBData::TB_REGISTRY, array(
        'value' => $value,
        'name' => $name
    ), array('name' => $prevName));
    
    $log->record('REG', 'Edited item ('.$name.')', $usr->getID());
    FormSubmission::submit('registry', FRM_SUBM_SUCCESS, 'Changes saved.', 'index.php?c=sc&a=registry');
}
?>
<form class="well form-inline" action="index.php?c=sc&amp;a=registry&amp;edit=<?= $sys->catchGetVar('edit'); ?>" method="post" name="registry_item_edit">
    <a class="close" href="index.php?c=sc&amp;a=registry">&times;</a>
    
    <div class="input-prepend">
        <span class="add-on">Name</span><input class="span2" id="reg_item_name" name="reg_item_name" type="text" value="<?= $row->name; ?>" />
        <span class="add-on">Value</span><input class="span2" id="reg_item_value" name="reg_item_value" type="text" value="<?= $row->value; ?>" /> 
       <?php
        if($usr->isRoot()) {
        ?>
        <label class="checkbox">
            <input type="checkbox" name="reg_item_locked" value="1" <?= ($row->locked == 1) ? 'checked="checked"' : ''; ?> /> Locked
        </label>
        &nbsp;&nbsp;
        <?php
        }
        ?>
    </div>
    <button type="submit" class="btn btn-warning" name="reg_item_edit">Save changes</button>
</form>