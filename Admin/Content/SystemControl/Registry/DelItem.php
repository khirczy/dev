<?php
$db->query("SELECT *
            FROM ".DBData::TB_REGISTRY."
            WHERE id = '".$db->escape($sys->catchGetVar('del'))."'");
if($db->isAffected()) {
    $row = $db->fetchObj();
    
    $del = false;
    if($row->locked == 1) {
        if($usr->isRoot())
            $del = true;
    } else {
        $del = true;
    }
    
    if($del) {
        $log->record('REG', 'Deleted item ('.$row->name.')', $usr->getID());
        $db->delete(DBData::TB_REGISTRY, array('id' => $row->id));
        header("Location: index.php?c=sc&a=registry"); // force reload
    }
} else {
    // item not found
}