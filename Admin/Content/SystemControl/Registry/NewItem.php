<?php
if($sys->catchPostEvent('reg_item_save')) {
    $name = $sys->getPostValue('reg_item_name');
    $value = $sys->getPostValue('reg_item_value');
    
    $locked = 0;
    if($usr->isRoot() && $sys->getPostValue('reg_item_locked') == 1) {
        $locked = 1;
    }
    
    if($name != '') {
        $db->query("SELECT name
                    FROM ".DBData::TB_REGISTRY."
                    WHERE name = '".$name."'");
        if(!$db->isAffected()) {
            $db->insert(DBData::TB_REGISTRY, array(
                'name' => $name,
                'value' => $value,
                'locked' => $locked
            ));
            
            $log->record('REG', 'Added new item ('.$name.')', $usr->getID());
            FormSubmission::submit('registry', FRM_SUBM_SUCCESS, 'Saved.', 'index.php?c=sc&a=registry');
        } else {
            FormSubmission::error('Name already in use.');
        }
    } else {
        FormSubmission::error('Invalid input.');
    }
}
?>
<form class="well form-inline" action="index.php?c=sc&amp;a=registry" method="post" name="registry_item_add">
    <div class="input-prepend">
        <span class="add-on">Name</span><input class="span2" id="reg_item_name" name="reg_item_name" type="text" />
        <span class="add-on">Value</span><input class="span2" id="reg_item_value" name="reg_item_value" type="text" />
        <?php
        if($usr->isRoot()) {
        ?>
        <label class="checkbox">
            <input type="checkbox" name="reg_item_locked" value="1"> Locked
        </label>
        
        &nbsp;&nbsp;
        <?php
        }
        ?>
    </div>
    <button type="submit" class="btn" name="reg_item_save">Save</button>
</form>