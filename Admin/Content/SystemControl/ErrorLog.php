<?php
FormSubmission::showContainer('elog');

if($sys->catchGetVar('truncate', 'true')) {
    Error::truncateLogFile();
    $log->record('SC', 'Cleared error log file', $usr->getID());
    FormSubmission::submit('elog', FRM_SUBM_SUCCESS, 'Done.', 'index.php?c=sc&a=elog');
}

if(filesize(Error::$m_LogFile)) {
    $handle = fopen(Error::$m_LogFile, 'r');

    $i = 1;
    while($ln = fgets($handle, 1024)) {
        // date|25 ip|15 type|8 url|45 msg|0
        $date   = trim(substr($ln, 0, 25));
        $client = trim(substr($ln, 25, 15));
        $type   = trim(substr($ln, 40, 8));
        $url    = trim(substr($ln, 48, 45));
        $msg    = trim(substr($ln, 90));

        echo '<pre>
    No.:    '.$i.'
    Date:   '.$date.'
    Client: '.$client.'
    Type:   '.$type.'
    URL:   '.$url.'

    '.$msg.'
        </pre>';

        $i++;
    }
} else {
    echo '<div align="center">No errors in log. Yaaay!</div>';
}
?>

<div align="right">
    <a class="btn btn-danger" href="index.php?c=sc&amp;a=elog&truncate=true" onclick="return confirm('Sure?');"><i class="icon-trash icon-white"></i> Truncate</a>
</div>