<?php
if(!$sys->catchGetVar('a')) {
    /** redirect to system information on load */
    header("Location: index.php?c=sc&a=sysinfo");
}
?>
<div class="row">
    <div class="span4">
        <div class="well" style="padding: 8px 0;">
            <ul class="nav nav-list">
                <li class="nav-header">Information</li>
                
                <li<?= ($sys->catchGetVar('a', 'sysinfo') ? ' class="active"' : ''); ?>><a href="index.php?c=sc&amp;a=sysinfo"><i class="icon-black icon-leaf"></i> System information</a></li>
                <li<?= ($sys->catchGetVar('a', 'ulog') ? ' class="active"' : ''); ?>><a href="index.php?c=sc&amp;a=ulog"><i class="icon-black icon-time"></i> User log</a></li>
                <li<?= ($sys->catchGetVar('a', 'elog') ? ' class="active"' : ''); ?>><a href="index.php?c=sc&amp;a=elog"><i class="icon-black icon-exclamation-sign"></i> Error log</a></li>
                
                <li class="nav-header">Tools</li>
                
                <li<?= ($sys->catchGetVar('a', 'registry') ? ' class="active"' : ''); ?>><a href="index.php?c=sc&amp;a=registry"><i class="icon-black icon-cog"></i> Registry</a></li>
                <li<?= ($sys->catchGetVar('a', 'filevalidation') ? ' class="active"' : ''); ?>><a href="index.php?c=sc&amp;a=filevalidation"><i class="icon-black icon-file"></i> File Validation</a></li>
                <li<?= ($sys->catchGetVar('a', 'backup') ? ' class="active"' : ''); ?>><a href="index.php?c=sc&amp;a=backup"><i class="icon-black icon-inbox"></i> Backup</a></li>
            </ul>
        </div>
    </div>
    <div class="span8">
        <?php
        $router = new ActionRouter('a');
        $router->setRouterLabel('<div align="center">Choose wisely...</div>');
        
        $router->addAction('registry', 'Content/SystemControl/Registry.php');
        $router->addAction('elog', 'Content/SystemControl/ErrorLog.php');
        $router->addAction('ulog', 'Content/SystemControl/UserLog.php');
        $router->addAction('sysinfo', 'Content/SystemControl/SystemInformation.php');
        $router->addAction('filevalidation', 'Content/SystemControl/FileValidation.php');
        $router->addAction('backup', 'Content/SystemControl/Backup.php');
        
        $router->displayContent();
        ?>
    </div>
</div>