<?php
ob_start();

define('ROOT', realpath(__FILE__));
define('ROOT_DIR', str_replace('index.php', '', ROOT));

define('ADMIN_VERSION', '1.0.0');

/** use specific init */
define('DS', DIRECTORY_SEPARATOR);

define('DIR_APP', '../Application');

define('DIR_KERNEL', 'Kernel');
define('DIR_CONFIG', 'Config');
define('DIR_INTERFACES', 'Interfaces');
define('DIR_COMPONENTS', 'Components');
define('DIR_CACHE', 'Cache');
define('DIR_LANGUAGE', 'Language');
define('DIR_LIBRARY', 'Library');
define('DIR_MODULES', 'Modules');
define('DIR_THEME', 'Theme');

require DIR_APP . DS . DIR_KERNEL . DS . 'Kernel.php';
    new Kernel(false); // do not initialize controller.
                       // we'll use a specific controller

/* aliases */
$reg    = Kernel::$Registry;
$sys    = Kernel::$System;
$log    = Kernel::$Log;
$db     = Kernel::$Database;
$usr    = Kernel::$User;
$view   = Kernel::$View;

/* administrative classes and functions */
require 'Include/Classes/ActionRouter.Class.php';
require 'Include/Classes/Admin.Class.php';
require 'Include/Classes/FormSubmission.Class.php';
$adm = new Admin;

/** logout */
if($sys->catchGetVar('logout', 'true')) {
    $adm->logout();
}

/** init end */
if($adm->adminCookieExists()) {
    if($usr->getStatus() >= USR_MODERATOR)
        $file = 'Include/Overview.php';
    elseif($_COOKIE['id'] == USR_ROOT_ID) {
        if($_COOKIE['pwd'] == $reg->get('root_pw')) {
            $file = 'Include/Overview.php';
        } else {
            $adm->logout();
        }
    } else {
        $adm->logout();
    }
} else {
    $file = 'Include/Login.php';
}

require 'Layout/index.php';