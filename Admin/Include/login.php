        <?php if(!defined('ROOT')) header('Location: index.php'); ?>

        <style type="text/css">
            html, body {
                background-color: #eee;
            }
            body {
                padding-top: 40px; 
            }
            .container {
                width: 300px;
            }

            .container > .content {
                background-color: #fff;
                padding: 20px;
                margin: 0 -20px; 
                -webkit-border-radius: 10px 10px 10px 10px;
                -moz-border-radius: 10px 10px 10px 10px;
                        border-radius: 10px 10px 10px 10px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.15);
                        box-shadow: 0 1px 2px rgba(0,0,0,.15);
            }

            .login-form {
                margin-left: 65px;
            }

            legend {
                margin-right: -50px;
                font-weight: bold;
                color: #404040;
            }
        </style>
        
        <div class="container">
            <div class="content">
                <?php
                if(!isset($_COOKIE['login_tries'])) {
                    setcookie('login_tries', 0, CURRENT_TIMESTAMP + 900);
                }
                
                if($sys->catchPostEvent('send')) {
                    require 'Include/Classes/AdminLogin.Class.php';
                    new AdminLogin();
                    
                    if(AdminLogin::getError()) {
                        ?>
                        <div class="alert alert-error" align="center">
                            <?= AdminLogin::getError(); ?>
                        </div>
                        <?php
                    }
                }
                
                if(!isset($_COOKIE['login_lock'])) {
                    if(isset($_COOKIE['login_tries']) && $_COOKIE['login_tries'] > 0) {
                ?>
                <div class="alert" align="center">
                    <?= $_COOKIE['login_tries'] . '/' . $reg->get('acp_login_max_tries'); ?> Versuche verbraucht
                </div>
                <?php
                    }
                ?>
                <div class="row">
                    <div class="login-form">
                        <h2>Login</h2>
                        <form action="index.php" method="post" name="loginForm">
                            
                            <fieldset>
                                <div class="clearfix">
                                    <?php
                                    if(!$adm->adminCookieExists()) {
                                    ?>
                                    <input type="text" placeholder="Benutzername" name="acp_username" />
                                    <?php
                                    } else { // Admin cookie exists
                                    ?>
                                    <input type="text" placeholder="Benutzername" name="acp_username" value="John" />
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="clearfix">
                                    <input type="password" placeholder="Passwort" name="acp_password">
                                </div>
                                <button class="btn primary" type="submit" name="send">Einloggen</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <?php
                } else {
                ?>
                <div class="alert alert-error" align="center">
                    Sie haben alle verfügbaren Versuche zum Login aufgebraucht. Bitte warten Sie<br /><b>
                        <?php
                        echo $reg->get('acp_login_locktime') - (time() - $_COOKIE['login_lock']);
                        ?>
                    </b> Sekunden<br />bis Sie sich erneut einloggen können.
                </div>    
                <?php
                }
                ?>
            </div>
            
            <br />
            
            <div align="center" style="font-style: italic; font-size: 11px;">
                &copy; 2012 <b>DevCMS</b><br />
                Written by <b>Hirczy Kevin</b><br />
                All rights reserved<br /><br />
                Layout powered by Twitter Bootstrap
            </div>
        
        </div>
