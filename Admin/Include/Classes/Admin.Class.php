<?php
class Admin {
    private $m_FormError;
    private $m_FormSuccess;
    
    private $m_Content = array(
        'sc' => array(
            'file' => 'SystemControl.php',
            'title' => 'Systemsteuerung',
            'mod' => USR_ADMINISTRATOR
        ),
        'users' => array(
            'file' => 'Users.php',
            'title' => 'Benutzer',
            'mod' => USR_ADMINISTRATOR
        ),
        'dashboard' => array(
            'file' => 'Dashboard.php',
            'title' => '',
            'mod' => USR_MODERATOR
        ),
        'help' => array(
            'file' => 'Help.php',
            'title' => 'Hilfe',
            'mod' => USR_MODERATOR
        )
    );
    
    private $m_Breadcrumb = array(
        'Home' => 'index.php'
    );
    
    public function __construct() {
        $this->m_Breadcrumb['Home'] = 'index.php';
    }
    
    public function rootNotification() {
        if(Kernel::$User->isRoot())
            echo '<div class="alert alert-info">
                Sie sind als <b>root</b> eingeloggt.
            </div>';
    }
    
    public function errNotification() {
        if(Error::errorsInLog())
            echo '<div class="alert alert-error">
                Es befinden sich Einträge im <a href="index.php?c=sc&amp;a=elog">Errorlog</a>!
            </div>';
    }
    
    public function adminCookieExists() {
        if(isset($_COOKIE['acp_access']))
            return true;
        else
            return false;
    }
    
    public function deleteAdminCookie() {
        unset($_COOKIE['admin']);
        Kernel::$System->delCookies(array('admin'));
    }
    
    public function logout() {
        Kernel::$System->delCookies(array(
            'id', 'pwd', 'root', 'acp_access'
        ));
        header("Location: index.php");
    }
    
    public function getTitle() {
        if(isset($_GET['c'])) {
            $content = $_GET['c'];
            
            if(array_key_exists($content, $this->m_Content)) {
                return ' - '.$this->m_Content[$content]['title'];
            } else {
                return ' - Error';
            }
        }
    }
    
    public function userStatusLabel($uid) {
        global $usr;
        
        $status = $usr->getStatus($uid);
        
        switch($status) {
            case USR_REGISTERED:    return 'Benutzer';  break;
            case USR_MODERATOR:     return 'Moderator'; break;
            case USR_ADMINISTRATOR: return 'Administrator'; break;
            case USR_BANNED:        return 'Gesperrt';   break;
            default:                return 'Unbekannt'; break;
        }
    }
    
    public function loadContent() {
        global $db, $reg, $sys, $log, $usr, $adm;
        
        if(isset($_GET['c'])) {
            $content = $_GET['c'];
            
            if(array_key_exists($content, $this->m_Content)) {
                if($usr->getStatus() >= $this->m_Content[$content]['mod']) {
                    require 'Content/'. $this->m_Content[$content]['file'];
                } else
                    require 'Content/err-404.php';
            } else
                require 'Content/err-404.php';
        } else {
            /** include dashboard */
            require 'Content/Dashboard.php';
        }
    }
}