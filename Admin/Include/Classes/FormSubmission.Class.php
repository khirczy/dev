<?php
class FormSubmission {
    private $m_Error;
    
    public static function submit($name, $type, $msg, $redirect = '') {
        $_SESSION[$name] = $type . $msg;
        
        if($redirect != '')
            header("Location: ".$redirect);
    }
    
    public static function error($msg) {
        echo '<div class="alert alert-error">
            <button class="close" data-dismiss="alert">×</button>
            '.$msg.'
        </div>';
    }
    
    public static function showContainer($name) {
        global $sys;
        
        if(isset($_SESSION[$name])) {
            $errType = $_SESSION[$name][0];
            
            switch($errType) {
                case FRM_SUBM_SUCCESS:
                    echo '<div class="alert alert-success">
                            <button class="close" data-dismiss="alert">×</button>
                            
                            '.substr($_SESSION[$name], 1).'
                        </div>';
                    break;
                
                case FRM_SUBM_WARNING:
                    echo '<div class="alert">
                            <button class="close" data-dismiss="alert">×</button>
                            
                            '.substr($_SESSION[$name], 1).'
                        </div>';
                    break;
                
                default:
                    echo '<div class="alert">
                            <button class="close" data-dismiss="alert">×</button>
                            
                            '.substr($_SESSION[$name], 1).'
                        </div>';
                    break;
                    
                    Error::trigger('FRMS', 'Form submitted without type', PRIO_L);
                    break;
            }
            
            /** delete session after showing message */
            unset($_SESSION[$name]);
        } elseif(!isset($_SESSION) && $sys->catchPostEvent('')) {
            
        }
    }
}