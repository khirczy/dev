<?php
final class AdminLogin extends Database implements DBData {
    private $m_UserName;
    private $m_UserPass;
    
    private $m_UserID = 0;
    
    private static $m_LoginError;
    
    private $m_RootPw;
    private $m_MaxLoginTries;
    private $m_Locktime;
    private $m_LogTime;
    
    private function loginAsRoot() {
        if(md5($this->m_UserPass) == $this->m_RootPw) {
            Kernel::$System->setCookies(array(
                'id' => USR_ROOT_ID,
                'pwd' => md5($this->m_UserPass),
                'root' => true,
                'acp_access' => true
            ), $this->m_LogTime);
            
            Kernel::$Log->record('ACP', 'Client ('.CLIENT_IP.') logged in as root', USR_ROOT_ID);
            
            /** remove lock associated cookies */
            Kernel::$System->delCookies(array(
                'login_tries', 'login_lock'
            ));
            
            header("Location: index.php"); // TODO: force reload
        } else {
            Kernel::$System->delCookies(array(
                'id', 'pwd', 'root', 'acp_access'
            ));
            
            $this->setError('Das eingegebene Passwort ist falsch!');
        }
    }
    
    private function setLockCookie() {
        if(isset($_COOKIE['login_tries']) &&
                 $_COOKIE['login_tries'] >= $this->m_MaxLoginTries) {
            if(!isset($_COOKIE['login_lock'])) {
                Kernel::$System->setCookies(array('login_lock' => CURRENT_TIMESTAMP), $this->m_Locktime);
                if($this->m_UserID != 0)
                    Kernel::$Log->record('ACP', 'Login locked for user ('.$this->m_UserName.')', $this->m_UserID);
                else
                    Kernel::$Log->record('ACP', 'Login locked for client ('.CLIENT_IP.')');
                
                header("Location: index.php"); // TODO: force reload
            }
        }
    }
    
    private function increaseTryCounter() {
        if(isset($_COOKIE['login_tries']) && $_COOKIE['login_tries'] <= $this->m_MaxLoginTries) {
            setcookie('login_tries', $_COOKIE['login_tries'] + 1, CURRENT_TIMESTAMP + $this->m_MaxLoginTries);
        }
        
        $this->setLockCookie();
    }
    
    private function loginAsUser() {
        parent::query("SELECT id, name, status, password
                       FROM ".DBData::TB_USERS."
                       WHERE name = '".parent::escape($this->m_UserName)."'");
        if(parent::isAffected()) {
            $row = parent::fetchObj();
            
            $this->m_UserID = $row->id;
            if(md5($this->m_UserPass) == $row->password) {
                if($row->status >= USR_MODERATOR) {
                    Kernel::$System->setCookies(array(
                        'id' => $row->id,
                        'pwd' => md5($this->m_UserPass),
                        'acp_access' => true
                    ), $this->m_LogTime);
                    
                    Kernel::$Log->record('ACP', 'User logged into administration', $row->id);

                    header("Location: index.php"); // TODO: force reload
                } else {
                    $this->setError('Der Benutzer konnte nicht gefunden werden!');
                }
            } else {
                $this->setError('Das eingegebene Passwort ist falsch!');
            }
        } else {
            $this->setError('Der Benutzer konnte nicht gefunden werden!', false);
        }
    }
    
    private function setError($errMsg, $increaseTryCounter = true, $sleep = 2) {
        self::$m_LoginError = $errMsg;
        if($increaseTryCounter)
            $this->increaseTryCounter ();
        
        if($sleep > 0)
            sleep($sleep);
    }
    
    public static function getError() {
        return self::$m_LoginError;
    }
    
    function __construct() {
        global $reg;
        
        $this->m_RootPw = $reg->get('root_pw');
        $this->m_Locktime = $reg->get('acp_login_locktime');
        $this->m_MaxLoginTries = $reg->get('acp_login_max_tries');
        
        $this->m_LogTime = $reg->get('acp_login_logtime');
        
        $this->m_UserName = trim($_POST['acp_username']);
        $this->m_UserPass = trim($_POST['acp_password']);
        
        if($this->m_UserName != '' && $this->m_UserPass != '') {
            if($this->m_UserName == USR_ROOT_NAME) {
                $this->loginAsRoot();
            } else {
                $this->loginAsUser();
            }
        }
    }
}