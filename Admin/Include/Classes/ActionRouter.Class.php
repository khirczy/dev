<?php
final class ActionRouter {
    private $m_CurrentAction = '';
    
    private $m_ActionParam;
    private $m_Actions = array();
    private $m_DefaultAction;
    
    private $m_E404 = 'Content/err-404.php';
    
    private $m_RouterLabel;
    
    public function __construct($actionParam) {
        $this->m_ActionParam = $actionParam;
        if(isset($_GET[$actionParam])) {
            $this->m_CurrentAction = $_GET[$actionParam];
        }
    }
    
    public function setDefaultAction($file) {
        $this->m_DefaultAction = $file;
    }
    
    public function setRouterLabel($label) {
        $this->m_RouterLabel = $label;
    }
    
    public function addAction($actionName, $file) {
        $this->m_Actions[$actionName] = $file;
    }
    
    public function displayContent() {
        global $db, $reg, $sys, $log, $usr, $adm;
        
        if($this->m_CurrentAction != '') {
            if(array_key_exists($this->m_CurrentAction, $this->m_Actions)) {
                if(file_exists($this->m_Actions[$this->m_CurrentAction]))
                    require $this->m_Actions[$this->m_CurrentAction];
                else {
                    require $this->m_E404;
                }
            } else {
                require $this->m_E404;
            }
        } else {
            if($this->m_DefaultAction != '')
                require $this->m_DefaultAction;
            else {
                if($this->m_RouterLabel != '')
                    echo $this->m_RouterLabel;
                else
                    require $this->m_E404;
            }
        }
    }
}