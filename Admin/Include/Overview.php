        <?php
        if(!defined('ROOT')) header('Location: index.php');
        
        $scCheck = '';
        if(!isset($_SESSION['sc'])) {
            $scCheck = 'onclick="return confirm(\'Die Systemsteuerung ist ein Ort voller Gefahren, Monstern und purem Englisch. Wollen Sie wirklich fortfahren? (Dieser Text möge irgendwann angepasst werden...)\');"';
            $_SESSION['sc'] = true;
            $log->record('ACP', 'Entered system control', $usr->getID());
        }
        ?>

        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="index.php">Administration</a>
                    <div class="nav-collapse">
                        <ul class="nav">
                            <li<?= (!$sys->catchGetVar('c') ? ' class="active"' : ''); ?>><a href="index.php">Dashboard</a></li>
                            <li<?= ($sys->catchGetVar('c', 'users') ? ' class="active"' : ''); ?>><a href="index.php?c=users">Benutzer</a></li>
                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Module <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                </ul>
                            </li>
                            <li<?= ($sys->catchGetVar('c', 'settings') ? ' class="active"' : ''); ?>><a href="index.php?c=settings">Einstellungen</a></li>
                            <li<?= ($sys->catchGetVar('c', 'help') ? ' class="active"' : ''); ?>><a href="index.php?c=help">Hilfe</a></li>
                            <li<?= ($sys->catchGetVar('c', 'sc') ? ' class="active"' : ''); ?>><a href="index.php?c=sc" <?=$scCheck;?>>Systemsteuerung</a></li>
                        </ul>
                        <ul class="nav pull-right">
                            <li class="divider-vertical"></li>
                            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="icon-user"></i> <?= $usr->get('name'); ?>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
<!--                                <li><a data-toggle="modal" href="#myModal">Profile</a></li>-->
<!--                                <li class="divider"></li>-->
                                <li><a href="index.php?logout=true">Ausloggen</a></li>
                            </ul>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>

        <div class="container">
            <?php
            $adm->rootNotification();
            $adm->errNotification();
            
            $adm->loadContent();
            ?>

        </div> <!-- /container -->

        <script src="Layout/assets/js/bootstrap.js"></script>