<?php define('LOAD_OPTIONS', true); include 'Options.php'; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><? echo $t_PageTitle; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <meta name="description" content="<? echo $t_Meta_Description; ?>" />
        <meta name="author" content="<? echo $t_Meta_Author; ?>" />
        <meta name="keywords" content="<? echo $t_Meta_Keywords; ?>" />
        <meta name="copyright" content="<? echo $t_Meta_Copyright; ?>" />
        <meta name="robots" content="<? echo $t_Meta_Robots; ?>" />
        <meta http-equiv="content-type" content="text/html; charset=<? echo $t_Meta_Charset; ?>" />

        <link href="../Theme/assets/css/bootstrap.css" rel="stylesheet">
        
        <style>
            body {
                padding-top: 30px;
            }
        </style>
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    
    <body>
        <div class="container">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="#"><? echo $t_SiteName ?></a>
                        <div class="nav-collapse">
                            <ul class="nav">
                                <? echo $t_Navigation; ?>
                            </ul>
                            <form class="navbar-search pull-left" action="">
                                <input type="text" class="search-query span2" placeholder="Search">
                            </form>
                            <ul class="nav pull-right">
                                <li><a href="#">Link</a></li>
                                <li class="divider-vertical"></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <? $view->showContent(); ?>
            
        </div>

<!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>-->
        <script src="../Theme/assets/js/bootstrap.js"></script>
    </body>
</html>
