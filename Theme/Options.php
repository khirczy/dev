<?php
if(defined('LOAD_OPTIONS')) {
    $t_PageTitle        = $view->getPageTitle();
    $t_SiteName         = $view->getSiteName();
    $t_Navigation       = $view->showNavigation();
    
    $t_Meta_Description = $reg->get('meta_description');
    $t_Meta_Author      = $reg->get('meta_author');
    $t_Meta_Keywords    = $reg->get('meta_keywords');
    $t_Meta_Charset     = $reg->get('meta_charset');
    $t_Meta_Copyright   = $reg->get('meta_copyright');
    $t_Meta_Robots      = $reg->get('meta_robots');
}