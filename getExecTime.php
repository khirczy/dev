<?php

// just for development
$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$endtime = $time;
$totaltime = ($endtime - Kernel::$scriptExecutionStart);
echo '<hr noshade size="1" />Interpreted in ' . $totaltime . ' seconds.';

$log = 'ExecTime.log';
if ($handle = fopen($log, 'a+')) {
    $content = date('d.m.Y') . "\t" . date('H:i:s') . "\t" . round($totaltime, 12)."\n";

    fwrite($handle, $content);
}

if(Error::errorsInLog())
    echo '<br /><b>Errors in log!</b>';